import json
import time
from attrdict import AttrDict   # FIXME tiny pip dependency
import numpy as np
from theano import tensor as T
import cPickle as pickle
from utils import load_model_params
from run_utils import setup_exp, get_logger
from encdec_shared import RNNEncoder, RNNDecoder, EncoderDecoder, reverse_sent
from char_fuel import load_parallel_data
from char_encdec import seq_to_str, clip_lengths, pyrpad
from os.path import join as pjoin
from cfg import CLIP_RATIO, CHAR_MAX_SEQ_LEN, CHAR_EOS_TOK, CHAR_SOS_TOK,\
    DECODER_LENGTH_ABS_DIFF, DECODER_MIN_LENGTH_RATIO, DECODER_MAX_LENGTH_RATIO
from collections import Counter

# runs for single example, batched over beam size
def beam_decoder(encdec, bdict, fdict, hs, seq_len, orig_sent, lm, beam_size=16, alpha=0.25, beta=0.0):
    #print('using beam: %d' % beam_size)
    CHAR_SOS_IND = fdict[CHAR_SOS_TOK]
    CHAR_EOS_IND = fdict[CHAR_EOS_TOK]
    N = len(fdict)

    # keep track of candidate:
    # - hypothesis (sentence)
    # - hidden states
    # - cumulative log probability (to prevent underflow)
    prev_lprobs = [0.0]
    prev_h_ps = [np.zeros(hs[0].shape, dtype=np.float32) for k in xrange(encdec.rlayers)]
    prev_hyps = ['']

    # hypotheses that finish early in search
    finished_hyps = list()
    finished_lprobs = list()

    s_t = np.array([CHAR_SOS_IND,], dtype=np.int32)
    # NOTE just constraining search to multiple of length of input sequence
    for j in xrange(max(DECODER_LENGTH_ABS_DIFF, int(DECODER_MAX_LENGTH_RATIO * seq_len))):
        next_lprobs = list()
        next_h_ps = list()
        next_hyps = list()

        # compute probability of next characters

        if j == 0:
            stacked_hs = hs
        elif j == 1:
            stacked_hs = np.concatenate([hs] * min(N, beam_size), axis=1)
        elif j == 2:
            stacked_hs = np.concatenate([hs] * beam_size, axis=1)
        else:
            pass
        ret = encdec.decode_step(*([s_t] + prev_h_ps + [stacked_hs]))
        # batch size by |V|
        p_t = ret[0].astype(np.float64)
        p_t = p_t / np.sum(p_t, axis=1, keepdims=True)
        h_ps = ret[1:-1]
        align = ret[-1]  # ignoring for now

        # update book-keeping

        next_lprobs = np.concatenate([prev_lprobs[k] + np.log(p_t[k]) for k in xrange(len(prev_lprobs))])
        PARTITION = -min(2 * beam_size, next_lprobs.shape[0])
        max_inds = np.argpartition(next_lprobs, PARTITION)[PARTITION:].flatten()[::-1]
        next_hyps = [prev_hyps[ind / N] + bdict[ind % N] for ind in max_inds]
        next_lprobs = next_lprobs[max_inds]

        # extract completed sentences and apply language model where appropriate

        pruned_subset = list()
        pruned_inds = list()
        for k, ind in enumerate(max_inds):
            if ind % N == CHAR_EOS_IND and j != 0:
                # see case where encounter space for explanation
                # difference is no space before <eos>
                lm_last_score = 0
                finished_hyps.append(next_hyps[k])
                finished_lprobs.append(next_lprobs[k] + alpha * lm_last_score)
            else:
                pruned_subset.append(k)
                pruned_inds.append(ind)
            if ind % N == fdict[' '] and j != 0 and next_hyps[k].strip() != '':
                lm_last_score = 0
                next_lprobs[k] = next_lprobs[k] + alpha * lm_last_score

        # update book-keeping again

        next_lprobs = next_lprobs[pruned_subset]
        next_hyps = [next_hyps[l] for l in pruned_subset]
        for k in xrange(len(h_ps)):
            h_p = np.vstack([h_ps[k][ind / N] for ind in pruned_inds])
            next_h_ps.append(h_p)
        s_t = np.array([ind % N for ind in pruned_inds], dtype=np.int32)

        # sort by probability, apply beam, and reform
        # first need to add in word bonus
        beam_lprobs = [next_lprobs[k] + beta * (next_hyps[k].count(' ') + 1) for k in xrange(len(next_lprobs))]
        sorted_inds = np.argsort(beam_lprobs)[::-1][:beam_size]
        next_lprobs = [next_lprobs[k] for k in sorted_inds]
        next_hyps = [next_hyps[k] for k in sorted_inds]
        next_h_ps = [h_p[sorted_inds] for h_p in next_h_ps]
        s_t = s_t[sorted_inds]

        # more book-keeping

        prev_h_ps = next_h_ps
        prev_lprobs = next_lprobs
        prev_hyps = next_hyps

    # sort finished hyps and probs and return the best
    # remember to apply word bonus again
    if len(finished_hyps) == 0:
        finished_hyps = next_hyps
        finished_lprobs = next_lprobs
    #hyps_and_lprobs = sorted(zip(finished_hyps, finished_lprobs), key=lambda x: (x[1] + beta * (x[0].count(' ') + 1)) / len(x[0]), reverse=True)
    hyps_and_lprobs = sorted(zip(finished_hyps, finished_lprobs), key=lambda x: (x[1] + beta * (x[0].count(' ') + 1)) / (x[0].count(' ') + 1), reverse=True)
    top_sent = hyps_and_lprobs[0][0]
    if top_sent.endswith(CHAR_EOS_TOK):
        top_sent = top_sent[:-len(CHAR_EOS_TOK)]
    return top_sent, None

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('expdir', type=str, default='sandbox', help='experiment directory to load from')
    parser.add_argument('model', type=str, help='model file to use')
    parser.add_argument('outfile', type=str, help='file to write decoder output sentences to')
    parser.add_argument('--inpfile', type=str, help='input file to decode')
    parser.add_argument('--beam', type=int, default=12, help='beam size to use')
    parser.add_argument('--test', action='store_true', help='evaluate on test set instead of dev set')
    parser.add_argument('--alpha', type=float, default=0.1, help='language model weight')
    parser.add_argument('--beta', type=float, default=0.0, help='word insertion bonus')
    args = parser.parse_args()
    print(args)

    # get options from run
    args.resume_epoch = False
    logger = get_logger('.')
    with open(pjoin(args.expdir, 'opts.json'), 'r') as fin:
        opts = json.load(fin)
        if 'pyramid' not in opts:
            opts['pyramid'] = False
        opts = AttrDict(opts)

    # load data

    logger.info('setting up data...')
    with open(pjoin(args.expdir, 'dicts.pk'), 'rb') as fin:
        dicts = pickle.load(fin)
        fdict, bdict = dicts['fdict'], dicts['bdict']
    bsize = opts.batch_size / CLIP_RATIO
    if args.inpfile:
        stream = load_parallel_data(args.inpfile, args.inpfile, bsize, 1, fdict, training=False)
    logger.info('done setting up data')

    # load lm and encdec model

    lm = None

    logger.info('loading model...')
    encdec = EncoderDecoder(opts, attention=opts.attention, bidir=opts.bidir, subset_grad=False, pyramid=opts.pyramid)
    load_model_params(encdec, args.model)
    logger.info('done loading model')

    # finally, run decoding

    decoded_sentences = list()
    original_sentences = list()
    decode_costs = []
    lengths = []

    sources = list()
    alignments = list()

    logger.info('starting decoding')
    tic = time.time()

    def decode_fn(model, bdict, fdict, hs, sslen, orig_sent, lm):
        return beam_decoder(model, bdict, fdict, hs, sslen, orig_sent, lm,\
                beam_size=args.beam, alpha=args.alpha, beta=args.beta)

    for ss, sm, ts, tm in stream.get_epoch_iterator():
        ss, sm = pyrpad(ss, sm, opts.rlayers, opts.pyramid)
        ss, ts = ss.astype(np.int32), ts.astype(np.int32)
        sm, tm = sm.astype(np.bool), tm.astype(np.bool)
        rss = reverse_sent(ss, sm)
        #ss, sm = clip_lengths(ss, sm)
        cost = encdec.test(ss, sm, rss, ts, tm, None)
        decode_costs.append(cost * tm.shape[0])
        total_length = np.sum(tm[:, 1:])
        lengths.append(total_length)

        # decode / save data for visualization

        for k in xrange(ss.shape[0]):  # over batch
            ssk, smk, tsk, tmk = ss[k:k+1], sm[k:k+1], ts[k:k+1], tm[k:k+1]
            rssk = reverse_sent(ssk, smk)
            sslen = np.sum(smk)
            orig_sent = seq_to_str(ssk[0, 0:sslen].tolist(), bdict).strip()
            tmp = encdec.encode(ssk, rssk, smk, None)
            # NOTE assumes attention
            _, hs = tmp[:-1], tmp[-1]
            sent, alignment = decode_fn(encdec, bdict, fdict, hs, sslen, orig_sent, lm)

            decoded_sentences.append(sent)
            original_sentences.append(orig_sent)
            alignments.append(alignment)
            sources.append(ssk)
            logger.info(seq_to_str(ssk[0, 0:sslen].tolist(), bdict) + ' | ' + sent)

    toc = time.time()
    logger.info('decoding time: %fs' % (toc - tic))
    logger.info('average cost: %f' %\
            (sum(decode_costs) / float(sum(lengths))))

    # write to output file

    with open(args.outfile, 'w') as fout:
        fout.write('\n'.join(decoded_sentences))
