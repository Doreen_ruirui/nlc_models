import os
import string
from os.path import join as pjoin

TRAIN_FILES = ['YOUR SRC FILE HERE', 'YOUR TGT FILE HERE']
VALID_FILES = ['YOUR SRC FILE HERE', 'YOUR TGT FILE HERE']

'''
character models
'''

CHAR_SOS_TOK = '<sos>'
CHAR_EOS_TOK = '<eos>'
CHAR_UNK_TOK = '<unk>'
CHAR_MAX_SEQ_LEN = 200
CHAR_SORT_K_BATCHES = 12
# reduce batch sizes if using attention to avoid memory error
CLIP_RATIO = 2

'''
decoding
'''

DECODER_MAX_LENGTH_RATIO = 1.5
DECODER_MIN_LENGTH_RATIO = 1.5
DECODER_LENGTH_ABS_DIFF = 5
