import h5py
import random
import numpy as np
import theano
import theano.tensor as T
import cPickle as pickle
from collections import defaultdict
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import os

'''
helper functions
'''

rng = np.random.RandomState(1234)
floatX = theano.config.floatX

# dataset

def save_model_params(model, fname):
    f = h5py.File(fname, 'w')
    names = [str(p) for p in model.params]
    assert(len(set(names)) == len(names))
    for p in model.params:
        dset = f.create_dataset(str(p), data=p.get_value())
    f.close()

def load_model_params(model, fname):
    f = h5py.File(fname, 'r')
    names = [str(p) for p in model.params]
    for p, name in zip(model.params, names):
        p.set_value(f[name][:])
    f.close()

def ortho_init(ndim, ndim1, act=None, scale=None):
    assert(ndim == ndim1)
    W = np.random.randn(ndim, ndim)
    u, s, v = np.linalg.svd(W)
    return u.astype(floatX)

def norm_init(nin, nout, act=None, scale=0.01):
    W = scale * np.random.randn(nin, nout)
    return W.astype(floatX)

def uniform_init(nin, nout, act=None, scale=0.08):
    W = np.random.uniform(low=-1 * scale, high=scale, size=nin*nout)
    W = W.reshape((nin, nout))
    return W.astype(floatX)

def torch_init(nin, nout, act=None, scale=None):
    scale = 1. / np.sqrt(nout)
    return uniform_init(nin, nout, scale=scale)

def glorot_init(n_in, n_out, act=None, scale=None):
    W_values = np.asarray(
        rng.uniform(
            low=-np.sqrt(6. / (n_in + n_out)),
            high=np.sqrt(6. / (n_in + n_out)),
            size=(n_in, n_out)
        ),
        #rng.randn(n_in, n_out) * 0.01,
        dtype=floatX
    )
    if act == T.nnet.sigmoid:
        W_values *= 4
    return W_values

class Dropout:

    def __init__(self, inp, p):
        # NOTE need to set p to 0 during testing
        self.srng = RandomStreams(seed=np.random.randint(1e6))
        self.p = p
        self.inp = inp
        self.out = self.inp * self.srng.binomial(self.inp.shape, p=1.0 - self.p, dtype=floatX) / (1.0 - self.p)

def _linear_params(n_in, n_out, suffix, init=uniform_init, scale=None, bias=True, act=None):
    if scale == None:
        scale = float(os.environ.get('WEIGHT_SCALE', '0.1'))
    W = theano.shared(init(n_in, n_out, act=act, scale=scale), 'W_' + suffix, borrow=True)
    if bias:
        b = theano.shared(np.zeros((n_out,), dtype=floatX), 'b_' + suffix, borrow=True)
        return W, b
    else:
        return W

def to_one_hot(y, nclasses):
    if np.isscalar(y):
        ny = 1
        y = [y]
    else:
        ny = y.shape[0]
    y_1_hot = np.zeros((ny, nclasses), dtype=np.int32)
    for k in xrange(ny):
        y_1_hot[k, y[k]] = 1
    return y_1_hot
