import theano
import theano.tensor as T
from theano.tensor.nnet import categorical_crossentropy
import numpy as np
import itertools
from utils import floatX, _linear_params

def seq_cat_crossent(pred, targ, mask, normalize=False):
    # dim 0 is time, dim 1 is batch, dim 2 is category
    pred_flat = pred.reshape(((pred.shape[0] *
                               pred.shape[1]),
                               pred.shape[2]), ndim=2)
    targ_flat = targ.flatten()
    mask_flat = mask.flatten()
    ce = categorical_crossentropy(pred_flat, targ_flat)
    # normalize by batch size and seq length
    cost = T.sum(ce * mask_flat)
    if normalize:
        # normalize by batch and length
        cost =  cost / T.sum(mask_flat)
    else:
        # just normalize by batch size
        cost = cost / pred.shape[1]
    return cost

class SequenceLogisticRegression(object):

    # multi-class logistic regression class over sequence

    def __init__(self, inp, n_in, n_out):
        # initialize w/ zeros
        self.W, self.b = _linear_params(n_in, n_out, 'sm')
        E = T.dot(inp, self.W) + self.b
        # time, batch, cat (None just keeps dimension)
        E = T.exp(E - T.max(E , axis=2, keepdims=True))
        pmf = E / T.sum(E, axis=2, keepdims=True)
        self.p_y_given_x = pmf
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)
        self.out = self.p_y_given_x
        # parameters of the model
        self.params = [self.W, self.b]

class LayerWrapper(object):

    def __init__(self, out):
        self.out = out
        self.params = []

class RNN(object):

    def __init__(self, rlayers, olayer, cost=None, downscales=None):
        # e.g. GRULayer
        self.rlayers = rlayers
        # e.g. softmax or last state
        self.olayer = olayer
        # e.g. categorical cross-entropy over sequence outputs
        if cost:
            self.cost = cost
        else:
            self.cost = 0
        self.params = list(itertools.chain(*[r.params for r in self.rlayers])) +\
                self.olayer.params
        if downscales:
            for d in downscales:
                self.params = self.params + d.params
        self.out = self.olayer.out

class GRULayer(object):

    def __init__(self, x, mask, x_dim, outputs_info, args, suffix='', backwards=False):
        # NOTE if want to stack should equal hdim
        self.xdim = x_dim
        self.hdim = args.rnn_dim
        self.backwards = backwards
        # initialize parameters
        # TODO maybe try initialization here: https://github.com/kyunghyuncho/dl4mt-material/blob/master/session1/nmt.py, helps for memorizing long sequences
        self.W_z, self.b_wz = _linear_params(self.xdim, self.hdim, 'wz%s' % suffix, act=T.nnet.sigmoid)
        self.U_z, self.b_uz = _linear_params(self.hdim, self.hdim, 'uz%s' % suffix, act=T.nnet.sigmoid)
        self.W_r, self.b_wr = _linear_params(self.xdim, self.hdim, 'wr%s' % suffix, act=T.nnet.sigmoid)
        self.U_r, self.b_ur = _linear_params(self.hdim, self.hdim, 'ur%s' % suffix, act=T.nnet.sigmoid)
        self.W_h, self.b_wh = _linear_params(self.xdim, self.hdim, 'wh%s' % suffix)
        self.U_h, self.b_uh = _linear_params(self.hdim, self.hdim, 'uh%s' % suffix)
        self.setup(x, mask, outputs_info)

    def flip_nonpadding(self, x, mask):
        #x = theano.printing.Print(x)(x)
        lens = T.sum(mask, axis=0)
        def flip_step(l, k, h):
            # scan iterates batch size times
            tmp = h[:, k, :]
            tmp2 = tmp[:l].copy() # copy here to prevent weird "cycle in graph" theano error
            return k + 1, T.set_subtensor(tmp[:l], tmp2[::-1])
        pair, updates = theano.scan(flip_step, sequences=lens, outputs_info=[0, None], non_sequences=x)
        out = pair[1].dimshuffle((1, 0, 2)) * mask[:, :, None]
        #out = theano.printing.Print(out)(out)
        return out

    def setup(self, x, mask, outputs_info):
        self.params = [self.W_z, self.b_wz,
                       self.U_z, self.b_uz,
                       self.W_r, self.b_wr,
                       self.U_r, self.b_ur,
                       self.W_h, self.b_wh,
                       self.U_h, self.b_uh]
        rval, updates = theano.scan(self._step,
                sequences=x, outputs_info=outputs_info)
        # out should be of dim (sequence length, batch size, hidden size)
        self.out = rval * mask[:, :, None]
        # flip everything before the padding (since we want to be able to add forward and backward hidden states)
        if self.backwards:
            self.out = self.flip_nonpadding(self.out, mask)

    def _step(self, x, prev_h):
        z = T.nnet.sigmoid(T.dot(x, self.W_z) + self.b_wz +\
                           T.dot(prev_h, self.U_z) + self.b_uz)
        r = T.nnet.sigmoid(T.dot(x, self.W_r) + self.b_wr +\
                           T.dot(prev_h, self.U_r) + self.b_ur)
        h = T.tanh(T.dot(x, self.W_h) + self.b_wh +\
                   T.dot(r * prev_h, self.U_h) + self.b_uh)

        next_h = z * h + (1 - z) * prev_h
        return next_h

class GRULayerAttention(GRULayer):

    # assuming this will only be used in decoder hence no backwards option
    def __init__(self, hs, x, mask, x_dim, outputs_info, args, suffix=''):
        self.W_concat, self.b_concat = _linear_params(args.rnn_dim * 2, args.rnn_dim, 'concat%s' % suffix)
        self.W_att1, self.b_att1 = _linear_params(args.rnn_dim, args.rnn_dim, 'att1%s' % suffix)
        self.W_att2, self.b_att2 = _linear_params(args.rnn_dim, args.rnn_dim, 'att2%s' % suffix)
        self.hs = hs  # e.g. from encoder
        self.phi_hs = T.tanh(T.dot(self.hs, self.W_att1) + self.b_att1)
        super(GRULayerAttention, self).__init__(x, mask, x_dim, outputs_info, args, suffix=suffix)

    def setup(self, x, mask, outputs_info):
        self.params = [self.W_z, self.b_wz,
                       self.U_z, self.b_uz,
                       self.W_r, self.b_wr,
                       self.U_r, self.b_ur,
                       self.W_h, self.b_wh,
                       self.U_h, self.b_uh,
                       self.W_concat, self.b_concat,
                       self.W_att1, self.b_att1,
                       self.W_att2, self.b_att2]
        # NOTE this differs from lstm in that we take output from tanh(Wc[h, context]) for softmax
        ret, updates = theano.scan(self._step,
                sequences=x, outputs_info=[outputs_info, None], non_sequences=self.hs)
        self.out = ret[0]
        self.out = mask[:, :, None] * self.out

    def _step(self, x, prev_h, hs):
        # standard gru update
        z = T.nnet.sigmoid(T.dot(x, self.W_z) + self.b_wz +\
                           T.dot(prev_h, self.U_z) + self.b_uz)
        r = T.nnet.sigmoid(T.dot(x, self.W_r) + self.b_wr +\
                           T.dot(prev_h, self.U_r) + self.b_ur)
        h = T.tanh(T.dot(x, self.W_h) + self.b_wh +\
                   T.dot(r * prev_h, self.U_h) + self.b_uh)
        h = z * h + (1 - z) * prev_h

        # attention mechanism over hs
        # NOTE could move outside, don't think could easily optimize for speed boost
        gamma_h = T.tanh(T.dot(h, self.W_att2) + self.b_att2)
        weights = T.sum(self.phi_hs * gamma_h, axis=2, keepdims=True)  # sum over hidden dimension (dot product of next_h with hs for each batch index), weights seq_length x batch_size x 1
        # exponentiate and normalize
        weights = T.exp(weights - T.max(weights, axis=0, keepdims=True))
        weights = weights / (T.sum(weights, axis=0, keepdims=True) + 1e-6)  # avoid division by 0
        context = T.sum(hs * weights, axis=0)  # sum from 1 to T over hs
        next_h = T.nnet.relu(T.dot(T.concatenate([h, context], axis=1), self.W_concat) + self.b_concat)
        #next_h = T.tanh(T.dot(h + context, self.W_concat))

        return next_h, weights[:, :, 0]

class LSTMLayer(object):

    # follows http://arxiv.org/pdf/1409.2329v5.pdf

    def __init__(self, x, mask, x_dim, outputs_info, args, suffix=''):
        # NOTE if want to stack should equal hdim
        self.xdim = x_dim
        self.hdim = args.rnn_dim
        self.W_hli, self.b_hli = _linear_params(self.xdim, self.hdim, 'hli%s' % suffix)
        self.W_hti, self.b_hti = _linear_params(self.hdim, self.hdim, 'hti%s' % suffix)
        self.W_hlf, self.b_hlf = _linear_params(self.xdim, self.hdim, 'hlf%s' % suffix)
        self.W_htf, self.b_htf = _linear_params(self.hdim, self.hdim, 'htf%s' % suffix)
        self.W_hlo, self.b_hlo = _linear_params(self.xdim, self.hdim, 'hlo%s' % suffix)
        self.W_hto, self.b_hto = _linear_params(self.hdim, self.hdim, 'hto%s' % suffix)
        self.W_hlg, self.b_hlg = _linear_params(self.xdim, self.hdim, 'hlg%s' % suffix)
        self.W_htg, self.b_htg = _linear_params(self.hdim, self.hdim, 'htg%s' % suffix)
        self.params = [self.W_hli, self.b_hli,
                       self.W_hti, self.b_hti,
                       self.W_hlf, self.b_hlf,
                       self.W_htf, self.b_htf,
                       self.W_hlo, self.b_hlo,
                       self.W_hto, self.b_hto,
                       self.W_hlg, self.b_hlg,
                       self.W_htg, self.b_htg]
        rval, updates = theano.scan(self._step,
                sequences=x, outputs_info=outputs_info)
        # out should be of dim (sequence length, batch size, hidden size)
        self.out = rval[0] * mask[:, :, None]
        self.cell = rval[1] * mask[:, :, None]

    def _step(self, x, prev_h, prev_c):
        i = T.nnet.sigmoid(T.dot(x, self.W_hli) + self.b_hli +\
                           T.dot(prev_h, self.W_hti) + self.b_hti)
        f = T.nnet.sigmoid(T.dot(x, self.W_hlf) + self.b_hlf +\
                           T.dot(prev_h, self.W_htf) + self.b_htf)
        o = T.nnet.sigmoid(T.dot(x, self.W_hlo) + self.b_hlo +\
                           T.dot(prev_h, self.W_hto) + self.b_hto)
        g = T.tanh(T.dot(x, self.W_hlg) + self.b_hlg +\
                   T.dot(prev_h, self.W_htg) + self.b_htg)
        next_c = f * prev_c + i * g
        next_h = o * T.tanh(next_c)
        return next_h, next_c

# Used in PyramidRNN
class Downscale(object):

    def __init__(self, x, dim, suffix=''):
        # NOTE if want to stack should equal hdim

        self.W, self.b = _linear_params(dim * 2, dim, 'ds%s' % suffix)

        # x.shape = [seq_len, batch_size, hdim]
        # x1.shape = [batch_size, seq_len / 2, hdim * 2]
        x1 = x.dimshuffle([1, 0, 2]).reshape([x.shape[1], x.shape[0]/2, x.shape[2] * 2])

        # x2.shape = [batch_size, seq_len / 2, hdim]
        x2 = x1.dot(self.W) + self.b

        # x3.shape = [seq_len / 2, batch_size, hdim]
        x3 = x2.dimshuffle([1, 0, 2])

        self.out = T.tanh(x3)

        self.params = [self.W, self.b]
