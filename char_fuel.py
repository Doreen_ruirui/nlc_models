import numpy as np
import string
from collections import Counter
from cfg import CHAR_MAX_SEQ_LEN, CHAR_EOS_TOK, CHAR_SOS_TOK, CHAR_UNK_TOK
from fuel.schemes import ConstantScheme
from fuel.streams import DataStream
from fuel.datasets.text import TextFile
from fuel.transformers import (
        Merge, Filter, Batch, SortMapping, Unpack, Padding, Mapping)

class TooLong(object):
    def __init__(self, seq_len):
        self.seq_len = seq_len
    def __call__(self, sents):
        # target sentence is artificially longer due to eos
        return len(sents[0]) <= self.seq_len

def seq_to_str(seq, bdict):
    return ''.join([bdict[k] for k in seq])

# also refer to machine_translation/stream.py
def load_parallel_data(src_file, tgt_file, batch_size, sort_k_batches, dictionary, training=False):
    def preproc(s):
        return s
    enc_dset = TextFile(files=[src_file], dictionary=dictionary,
            bos_token=None, eos_token=None, unk_token=CHAR_UNK_TOK, level='character', preprocess=preproc)
    dec_dset = TextFile(files=[tgt_file], dictionary=dictionary,
            bos_token=CHAR_SOS_TOK, eos_token=CHAR_EOS_TOK, unk_token=CHAR_UNK_TOK, level='character', preprocess=preproc)
    # NOTE merge encoder and decoder setup together
    stream = Merge([enc_dset.get_example_stream(), dec_dset.get_example_stream()],
            ('source', 'target'))
    if training:
        # filter sequences that are too long
        stream = Filter(stream, predicate=TooLong(seq_len=CHAR_MAX_SEQ_LEN))
        # batch and read k batches ahead
        stream = Batch(stream, iteration_scheme=ConstantScheme(batch_size*sort_k_batches))
        # sort all samples in read-ahead batch
        stream = Mapping(stream, SortMapping(lambda x: len(x[1])))
        # turn back into stream
        stream = Unpack(stream)
    # batch again
    stream = Batch(stream, iteration_scheme=ConstantScheme(batch_size))
    masked_stream = Padding(stream)
    return masked_stream
