import numpy as np
import difflib
from edit_dist import edit_distance
from colorama import Fore, Back
from collections import Counter

def disp_err_corr(hyp_corr, ref_corr, char=False):
    hyp_str = ''
    ref_str = ''
    assert len(hyp_corr) == len(ref_corr)
    for k in xrange(len(hyp_corr)):
        if hyp_corr[k] == '<ins>':
            hc = Back.GREEN + ' ' + Back.RESET
        else:
            hc = hyp_corr[k]

        if ref_corr[k] == '<del>':
            rc = Back.RED + ' ' + Back.RESET
        else:
            rc = ref_corr[k]

        if hc != rc and (not char or (len(hc) == 1 and len(rc) == 1)):
            hc = Back.BLUE + Fore.BLACK + hc + Fore.RESET + Back.RESET
            rc = Back.BLUE + Fore.BLACK + rc + Fore.RESET + Back.RESET
        hyp_str += hc
        ref_str += rc
        if not char and k < len(hyp_corr) - 1:
            hyp_str += ' '
            ref_str += ' '
    print hyp_str
    print ref_str

def update_counters(hyp_corr, ref_corr, ins_toks, del_toks, sub_toks):
    assert len(hyp_corr) == len(ref_corr)
    for k in xrange(len(hyp_corr)):
        hc = hyp_corr[k]
        rc = ref_corr[k]
        if hc == '<ins>':
            ins_toks[rc] += 1
        if rc == '<del>':
            del_toks[hc] += 1
        if hc != rc:
            sub_toks[(hc, rc)] += 1

def plot_errs_by_pos(err_by_pos, out_file):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    plt.plot(err_by_pos)
    #plt.show()
    plt.savefig(out_file)

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('hypfile', type=str, help='first file')
    parser.add_argument('reffile', type=str, help='second file')
    parser.add_argument('plotfile', type=str, help='file to save plots to')
    parser.add_argument('--display', action='store_true', help='display edit distance visualization in terminal')
    parser.add_argument('--char', action='store_true', help='character level instead of default world level')
    args = parser.parse_args()

    with open(args.hypfile, 'r') as fin:
        hyps = fin.read().split('\n')
        if not args.char:
            hyps = [s.split(' ') for s in hyps]
    with open(args.reffile, 'r') as fin:
        refs = fin.read().split('\n')
        if not args.char:
            refs = [s.split(' ') for s in refs]

    ''' difflib algo seems buggy/incompatible (doesn't minimize edit distance properly, doesn't stay intraline)
    htmldiff = difflib.HtmlDiff(wrapcolumn=80)
    with open(args.out, 'w') as fout:
        fout.write(htmldiff.make_file(lines1, lines2))
    '''

    hyp_lens = [len(s) for s in hyps]
    ref_lens = [len(s) for s in refs]
    max_hyp_len = max([len(hyp) for hyp in hyps])

    ins_toks = Counter()
    del_toks = Counter()
    sub_toks = Counter()

    tot_errs_by_pos = np.zeros(max_hyp_len)
    counts_by_pos = np.zeros(max_hyp_len, dtype=np.int32)

    tot_dist = tot_eq = tot_ins = tot_dels = tot_subs = 0.0
    num_sents_correct = 0
    correct_sents_len = 0

    for hyp, ref in zip(hyps, refs):
        dist, eq, ins, dels, subs, errs_by_pos, hyp_corr, ref_corr = edit_distance(hyp, ref)
        tot_eq += eq
        tot_ins += ins
        tot_dels += dels
        tot_subs += subs
        tot_errs_by_pos[0:errs_by_pos.shape[0]] += errs_by_pos
        counts_by_pos[0:errs_by_pos.shape[0]] += 1

        if dist == 0:
            num_sents_correct += 1
            correct_sents_len += len(ref)
        tot_dist += dist

        if args.display:
            disp_err_corr(hyp_corr, ref_corr, char=args.char)
            print

        update_counters(hyp_corr, ref_corr, ins_toks, del_toks, sub_toks)

    '''
    aggregate stats
    '''

    print 'avg len hyp: %f' % np.mean(hyp_lens)
    print 'avg len ref: %f' % np.mean(ref_lens)

    tot_comp_len = float(np.sum([max(h, r) for (h, r) in zip(hyp_lens, ref_lens)]))
    print 'frac eq: %f ins: %f del: %f sub: %f' %\
        tuple(np.array([tot_eq, tot_ins, tot_dels, tot_subs]) / tot_comp_len)

    print 'error rate: %f' % (100.0 * tot_dist / np.sum(np.sum(ref_lens)))

    print '%d/%d sents correct' % (num_sents_correct, len(hyps))
    print 'avg len of correct sent: %f' % (correct_sents_len / float(num_sents_correct))

    print
    for err_type, err_toks in zip(('ins', 'del', 'sub'), (ins_toks, del_toks, sub_toks)):
        print('most common %s' % err_type)
        for tok, count in err_toks.most_common(10):  # FIXME PARAM
            print '\t%s: %d' % (tok, count)

    '''
    more visualizations
    '''

    plot_errs_by_pos(tot_errs_by_pos / counts_by_pos, args.plotfile)

if __name__ == '__main__':
    main()
